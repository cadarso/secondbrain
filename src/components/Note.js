import { MdDeleteForever } from 'react-icons/md';
import { MdFileUpload } from 'react-icons/md';

const Note = ({ id, name, description, handleDeleteNote }) => {
	return (
		<div className='note'>
			<span>{description}</span>
			<div className='note-footer'>
				<MdDeleteForever
					onClick={() => handleDeleteNote(id)}
					className='delete-icon'
					size='1.3em'
				/>
				<MdFileUpload
					onClick={() => handleDeleteNote(id)}
					className='delete-icon'
					size='1.3em'
				/>
			</div>
		</div>
	);
};

export default Note;
