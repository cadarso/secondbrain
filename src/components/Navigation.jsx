import React from "react";
import { NavLink } from "react-router-dom";
import {Nav, Navbar, NavDropdown, MenuItem, Container, Tabs, ButtonToolbar, Button, Table, ButtonGroup, Row, Col, Grid, Panel, FormGroup, FormControl} from 'react-bootstrap';

function Navigation() {
  return (
    <div className="navigation">
		<Navbar expand="lg" variant="light" bg="light" sticky="top" collapseOnSelect>
			 <Container fluid>
			 <Navbar.Brand href="/">Quantified life</Navbar.Brand>
			 <Nav className="me-auto">
				 <Nav.Link href="/">Home</Nav.Link>
				 <Nav.Link href="/about">About</Nav.Link>
				 <Nav.Link href="/notesapp">Notes</Nav.Link>
			 </Nav>
			 </Container>
		 </Navbar>
    </div>
  );
}


export default Navigation;
