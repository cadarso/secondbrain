import React, { useState, useEffect } from 'react';
import './App.css';
import './index.css';
import Amplify, { API, graphqlOperation } from 'aws-amplify';
import awsconfig from './aws-exports';
import { withAuthenticator, AmplifySignOut } from '@aws-amplify/ui-react';
import { listNotes } from './graphql/queries';
import { createNote as createNoteMutation, deleteNote as deleteNoteMutation } from './graphql/mutations';
import { nanoid } from 'nanoid';
import NotesList from './components/NotesList';
import Search from './components/Search';
import Header from './components/Header';

Amplify.configure(awsconfig);

const initialFormState = { name: '', description: '' }

function App() {
  const [notes, setNotes] = useState([]);
  const [formData, setFormData] = useState(initialFormState);
  const [searchText, setSearchText] = useState('');
  const [darkMode, setDarkMode] = useState(false);

  useEffect(() => {
    fetchNotes();
  }, []);

  async function fetchNotes() {
    try {
      const apiData = await API.graphql(graphqlOperation(listNotes));
      const notesFromAPI = apiData.data.listNotes.items;
      console.log('note list', notesFromAPI);
      setNotes(notesFromAPI);
    } catch (error) {
        console.log('error on fetching notes', error);
    }
  }

  const addNote = async (text) => {
    const newNote = {
      id: nanoid(),
      name: text,
      description: text,
    };
    await API.graphql({ query: createNoteMutation, variables: { input: newNote } });
    setNotes([...notes, newNote]);
  };

  const deleteNote = async (id) => {
    const newNotes = notes.filter((note) => note.id !== id);
    setNotes(newNotes);
    await API.graphql({ query: deleteNoteMutation, variables: { input: { id } }});
  };

  return (
    <div className={`${darkMode && 'dark-mode'}`}>
			<div className='container' fluid>
				<Header handleToggleDarkMode={setDarkMode} />
				<Search handleSearchNote={setSearchText} />
				<NotesList
					notes={notes}
					handleAddNote={addNote}
					handleDeleteNote={deleteNote}
				/>
			</div>
    </div>
  );
}

export default withAuthenticator(App);
